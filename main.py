#!/usr/bin/env python

import logging
import configargparse
import threading
import json
import time
import signal
import enum

from typing import Dict, Any
from datetime import datetime, date

from data_reader import DataReader
from data_sender import DataSender

logger = logging.getLogger()

LOGGING_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'


class Encoder(json.JSONEncoder):

    def default(self, o: Any) -> str:  # pylint: disable=E0202
        if isinstance(o, enum.Enum):
            return o.name
        if isinstance(o, datetime):
            return o.strftime('%Y-%m-%d %H:%M:%S.%f')
        if isinstance(o, date):
            return o.strftime('%Y-%m-%d')
        return super(Encoder, self).default(o)


class DataProcessor:

    _shutdown_event: threading.Event = threading.Event()

    def __init__(self,
                 database_url: str,
                 database_user: str,
                 database_password: str,
                 database_schema: str,
                 cognito_app_client_id: str,
                 cognito_app_client_secret: str,
                 cognito_app_client_scope: str,
                 cognito_app_client_url: str,
                 api_gateway_url: str,
                 token_file_path: str,
                 site_name: str,
                 state_file_path: str,
                 batch_size: int,
                 send_interval: int,
                 error_interval: int):
        self._data_reader = DataReader(
            url=database_url,
            username=database_user,
            password=database_password,
            schema=database_schema
        )
        self._data_sender = DataSender(
            app_client_id=cognito_app_client_id,
            app_client_secret=cognito_app_client_secret,
            app_client_scope=cognito_app_client_scope,
            app_client_url=cognito_app_client_url,
            api_gateway_url=api_gateway_url,
            token_file_path=token_file_path
        )
        self._site_name = site_name
        self._state_file_path = state_file_path
        self._batch_size = batch_size
        self._send_interval = send_interval
        self._error_interval = error_interval
        self._sleep_delay = .01

    def _read_state(self) -> Dict[str, datetime]:
        """
        Reads state from JSON file.
        """

        try:
            f = open(self._state_file_path)
            return {table_name: datetime.fromisoformat(timestamp) for table_name, timestamp in json.load(f).items()}
        except FileNotFoundError:
            return {}

    def _write_state(self, state: Dict[str, datetime]):
        """
        Writes state to JSON file.
        """

        with open(self._state_file_path, 'w') as f:
            json.dump({table_name: datetime.isoformat(timestamp) for table_name, timestamp in state.items()}, f)

    def _process_data(self) -> bool:
        """
        Reads data with DataReader for each table and try to send it with DataSender.

        Returns False if there was no data to send.
        """

        send_data = []
        state = self._read_state()
        for table_name in self._data_reader.table_names:

            timestamp = state.get(table_name)
            data = self._data_reader.get_data(table_name=table_name, limit=self._batch_size, timestamp=timestamp)

            if not data['data']:
                logger.info(f"Table: '{table_name}', timestamp: '{timestamp}'. No rows found for sending.")
                continue

            send_data.append(data)
            timestamp = data['data'][-1]['timestamp']
            state[table_name] = timestamp
            logger.info(f"Table: '{table_name}', timestamp: '{timestamp}'. {len(data['data'])} rows found for sending.")

        if not send_data:
            return False

        serialized_data = json.dumps({'site': self._site_name, 'data': send_data}, cls=Encoder)
        self._data_sender.send_data(serialized_data)
        self._write_state(state)

        return True

    def start(self):
        """
        Starts data processing.
        """

        logger.info('Starting...')

        last_call_time = 0
        delay_interval = self._send_interval
        while not self._shutdown_event.is_set():

            time.sleep(self._sleep_delay)
            current_time = time.time()
            if current_time - last_call_time <= delay_interval:
                continue

            last_call_time = current_time
            try:
                if self._process_data():
                    delay_interval = 0
                else:
                    delay_interval = self._send_interval
            except Exception:
                logger.exception('An error occurred.')
                delay_interval = self._error_interval

    def stop(self):
        """
        Stops data processing.
        """

        logger.info('Shutting down...')
        self._shutdown_event.set()


def _signal_handler(signum, _frame):
    logger.debug(f'Got signal {signum}')
    data_processor.stop()

for sig in (signal.SIGINT, signal.SIGTERM):
    signal.signal(sig, _signal_handler)


if __name__ == '__main__':

    parser = configargparse.ArgumentParser(default_config_files=['config.ini'])

    parser.add_argument('--database-url', help='Database connection url in format '
                                               '"dialect[+driver]://host/dbname[?key=value..]"')
    parser.add_argument('--database-user', help='Database user')
    parser.add_argument('--database-password', help='Database password')
    parser.add_argument('--database-schema', help='Database schema')

    parser.add_argument('--cognito-app-client-id', help='Cognito app client id')
    parser.add_argument('--cognito-app-client-secret', help='Cognito app client secret')
    parser.add_argument('--cognito-app-client-scope', default='test-resource-service/scope',
                                                      help='Cognito app client scope')
    parser.add_argument('--cognito-app-client-url', help='Cognito app client url')
    parser.add_argument('--api-gateway-url', help='Cognito app client url')
    parser.add_argument('--token-file-path', default='./token.json', help='Token file path')

    parser.add_argument('--site-name', help='Site name')
    parser.add_argument('--state-file-path', default='./state.json', help='State file path')
    parser.add_argument('--batch-size', type=int, default=1000, help='How many rows to send at once from one table')
    parser.add_argument('--send-interval', type=int, default=60, help='How often to send data (in seconds)')
    parser.add_argument('--error-interval', type=int, default=1,  help='How long to wait before next attempt after '
                                                                       'failure (in seconds)')

    parser.add_argument('--log-level',
                        default=logging.INFO,
                        choices=logging._levelToName.values(),
                        help='Logging level')

    args = vars(parser.parse_args())
    logging.basicConfig(level=args.pop('log_level'), format=LOGGING_FORMAT)

    data_processor = DataProcessor(**args)
    data_processor.start()
