from typing import Tuple, Dict, Any, Optional
from enum import Enum
from datetime import datetime

import sqlalchemy as sa

from sqlalchemy import select
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine.url import make_url


__all__ = ['DataReader']


class LevelsEnum(Enum):
    """
    Levels enum.
    """

    asset = 1
    component = 2
    data_source = 3


class DataReader:
    """
    Reads data from data base.
    """

    _tables = {
        'shoe_sorter_data': {
            'level': LevelsEnum.asset
        },
        'divert_data': {
            'level': LevelsEnum.component
        },
        'divert_info': {
            'level': LevelsEnum.component
        },
        'divert_stats': {
            'level': LevelsEnum.component
        },
        'vfd_powerflex_data': {
            'level': LevelsEnum.component
        },
        'vfd_powerflex_info': {
            'level': LevelsEnum.component
        },
        'vfd_powerflex_stats': {
            'level': LevelsEnum.component
        },
        'vfd_powerflex_statuses': {
            'level': LevelsEnum.component
        },
        'banner_qm30vt2_data': {
            'level': LevelsEnum.data_source
        },
        'banner_qm30vt2_info': {
            'level': LevelsEnum.data_source
        },
        'ifm_vse151_data': {
            'level': LevelsEnum.data_source
        },
        'ifm_vse151_info': {
            'level': LevelsEnum.data_source
        }
    }

    _join_tables = {
        LevelsEnum.asset: [
            {
                'name': 'assets',
                'join_column_names': ('asset_id', 'id'),
                'column_names': (('name', 'asset_name'),)
            }
        ],
        LevelsEnum.component: [
            {
                'name': 'components',
                'join_column_names': ('component_id', 'id'),
                'column_names': (('location', 'component_location'),)
            },
            {
                'name': 'component_types',
                'to_join_table_name': 'components',
                'join_column_names': ('type_id', 'id'),
                'column_names': (('name', 'component_type_name'),)
            },
            {
                'name': 'assets',
                'to_join_table_name': 'components',
                'join_column_names': ('asset_id', 'id'),
                'column_names': (('name', 'asset_name'),)
            }
        ],
        LevelsEnum.data_source: [
            {
                'name': 'data_sources',
                'join_column_names': ('data_source_id', 'id'),
                'column_names': (('location', 'data_source_location'),)
            },
            {
                'name': 'data_source_types',
                'to_join_table_name': 'data_sources',
                'join_column_names': ('type_id', 'id'),
                'column_names': (('name', 'data_source_type_name'),)
            },
            {
                'name': 'components',
                'to_join_table_name': 'data_sources',
                'join_column_names': ('component_id', 'id'),
                'column_names': (('location', 'component_location'),)
            },
            {
                'name': 'component_types',
                'to_join_table_name': 'components',
                'join_column_names': ('type_id', 'id'),
                'column_names': (('name', 'component_type_name'),)
            },
            {
                'name': 'assets',
                'to_join_table_name': 'components',
                'join_column_names': ('asset_id', 'id'),
                'column_names': (('name', 'asset_name'),)
            }
        ]
    }

    def __init__(self, url: str, username: str, password: str, schema: str):
        url = make_url(url)
        url.username = username
        url.password = password
        engine = sa.create_engine(url)
        self._meta = sa.MetaData(bind=engine, schema=schema)
        self._session_maker = sessionmaker(bind=engine)

    @property
    def table_names(self) -> Tuple[str, ...]:
        """
        Returns table names.
        """

        return tuple(self._tables.keys())

    def get_data(self, table_name: str, limit: int, timestamp: Optional[datetime] = None) -> Dict[str, Any]:
        """
        Returns data.

        Example of return value:

        {
            'level': 'asset',
            'data': [
                {'air_pressure': 1, 'air_flow': 1, 'timestamp': datetime.datetime(2021, 3, 16, 16, 34, 23, 98520)}
            ]
        }
        """

        level = self._tables[table_name]['level']
        column_names = self._tables[table_name].get('column_names')
        join_table_options = self._join_tables[level]

        table_names = [table_name]
        for join_table_option in join_table_options:
            table_names.append(join_table_option['name'])

        self._meta.reflect(only=table_names)
        table = sa.Table(table_name, self._meta)
        if column_names:
            columns = [getattr(table.c, column_name) for column_name in column_names]
        else:
            columns = [table]

        for join_table_option in join_table_options:
            join_table = sa.Table(join_table_option['name'], self._meta)
            for column_name, new_column_name in join_table_option['column_names']:
                columns.append(getattr(join_table.c, column_name).label(new_column_name))

        query = select(columns)
        for join_table_option in join_table_options:

            join_table = sa.Table(join_table_option['name'], self._meta)
            if 'to_join_table_name' in join_table_option:
                to_join_table = sa.Table(join_table_option['to_join_table_name'], self._meta)
            else:
                to_join_table = table

            table_column_name, join_table_column_name = join_table_option['join_column_names']
            query = query.join(
                join_table,
                getattr(to_join_table.c, table_column_name),
                getattr(join_table.c, join_table_column_name)
            )

        query = select(columns).limit(limit).order_by(table.c.timestamp)
        if timestamp:
            query = query.where(table.c.timestamp > timestamp)

        result = self._session_maker().execute(query)
        keys = result.keys()
        rows = result.fetchall()
        data = [dict(zip(keys, row)) for row in rows]

        return {
            'level': level.name,
            'data': data
        }
