from typing import List, Dict, Any

import os
import base64
import json
import requests
import time


__all__ = ['DataSender']


class DataSenderException(IOError):
    """
    Data sender exception.
    """


class DataSenderAuthException(DataSenderException):
    """
    Data sender auth exception.
    """


class DataSender:
    """
    Encodes data to JSON and sends it to AWS cloud.
    """

    def __init__(self,
                 app_client_id: str,
                 app_client_secret: str,
                 app_client_scope: str,
                 app_client_url: str,
                 api_gateway_url: str,
                 token_file_path: str):
        self._app_client_id = app_client_id
        self._app_client_secret = app_client_secret
        self._app_client_scope = app_client_scope
        self._app_client_url = app_client_url
        self._api_gateway_url = api_gateway_url
        self._token_file_path = token_file_path

    def _auth_request(self) -> Dict[str, Any]:
        """
        Makes HTTP POST request to auth pool and returns response encoded body.
        """

        encode_data = f'{self._app_client_id}:{self._app_client_secret}'.encode('utf-8')
        encode_authorization = base64.b64encode(encode_data)
        authorization = encode_authorization.decode('utf-8')

        headers = {'Authorization': f'Basic {authorization}'}
        data = {'grant_type': 'client_credentials', 'scope': self._app_client_scope}

        response = requests.post(url=self._app_client_url, headers=headers, data=data)

        if response.status_code != 200:
            message = f'Invalid auth response status code: {response.status_code}'
            raise DataSenderAuthException(message)

        return json.loads(response.content)

    def _write_auth_data(self, auth_data: Dict[str, Any]):
        """
        Writes auth data.
        """

        with open(self._token_file_path, 'w') as f:
            json.dump(auth_data, f)

    def _get_access_token(self) -> str:
        """
        Gets access token from cache and returns it. It refreshes the token if it is already expired.
        """

        try:

            f = open(self._token_file_path)
            auth_data = json.load(f)
            mod_time = os.path.getmtime(self._token_file_path)
            if time.time() - mod_time + 5 > auth_data['expires_in']:
                auth_data = self._auth_request()
                self._write_auth_data(auth_data)

        except FileNotFoundError:
            auth_data = self._auth_request()
            self._write_auth_data(auth_data)

        return auth_data['access_token']


    def send_data(self, data: str):
        """
        Sends data.
        """

        access_token = self._get_access_token()
        headers = {'Authorization': f'Bearer {access_token}'}
        response = requests.post(self._api_gateway_url, headers=headers, data=data)
        if response.status_code != 200:
            message = f'Invalid response status code: {response.status_code}'
            raise DataSenderException(message)
